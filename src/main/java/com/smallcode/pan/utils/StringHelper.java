package com.smallcode.pan.utils;

import java.util.UUID;

public class StringHelper {
	
	///get uuid string
	public static final String GenerateGUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
