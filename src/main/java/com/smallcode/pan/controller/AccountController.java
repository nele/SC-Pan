package com.smallcode.pan.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.smallcode.pan.model.User;
import com.smallcode.pan.repository.UserRepo;

@Controller
public class AccountController {

	@Autowired
	private UserRepo userDao;
	
	//login page
	@RequestMapping(value="login.do",method=RequestMethod.GET)
	public String login(){
		return "./account/login";
	}
	
	//dologin
	@RequestMapping(value="login.do",method=RequestMethod.POST)
	public String login(String Username,String Password,Model model,HttpServletRequest request){
		 String returnMsg =  "";
		 if(Username.isEmpty() || Password.isEmpty()){
			 returnMsg = "用户名或密码不能为空";
		 }
		 else{
			 //login 
			 User user = userDao.DoLogin(Username, Password);
			  if(user==null){
				 returnMsg ="用户名或者密码错误！";
			  }
			  else{
				  request.getSession().setAttribute("user", user);
				  return "redirect:/index.do";
			  }
		 }
		 model.addAttribute("returnMsg", returnMsg);
		 return "./account/login";
	}
	
	//logoff
	@RequestMapping(value="logoff.do",method=RequestMethod.GET)
	public String logoff(HttpServletRequest request){
		 request.getSession().removeAttribute("user");
		 return "redirect:/login.do";
	}
}
