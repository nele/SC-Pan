package com.smallcode.pan.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.smallcode.pan.model.CloudFile;
import com.smallcode.pan.model.User;
import com.smallcode.pan.repository.CloudFileRepo;
import com.smallcode.pan.repository.UserRepo;
import com.smallcode.pan.utils.DateHelper;
import com.smallcode.pan.utils.FsHelper;
import com.smallcode.pan.utils.StringHelper;

@Controller
public class HomeController {

	@Autowired
	private UserRepo userDao;

	@Autowired
	private CloudFileRepo fileDao;

	@RequestMapping(value = "/index.do")
	public String index() {
		// List<User> users = (List<User>) userDao.findAll();
		// System.out.println(users.size());
		return "index";
	}

	@RequestMapping(value = "/addUser.do")
	public String add() {
		User user = new User();
		user.setUsername("admin");
		user.setPassword("123456");
		userDao.save(user);
		return "index";
	}

	@RequestMapping(value = "/test.do")
	public String test() {

		return "test";
	}

	// show files
	@RequestMapping(value = "file.do")
	public String file(Model model, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		List<CloudFile> files = (List<CloudFile>) fileDao.GetFilesByUser(user.getId());
		model.addAttribute("files", files);
		return "file";
	}

	@RequestMapping(value = "upload.do", method = RequestMethod.GET)
	public String upload() {
		return "upload";
	}

	// upload file
	@RequestMapping(value = "upload.do", method = RequestMethod.POST)
	public String upload(MultipartFile file, HttpServletRequest request) {
		CloudFile cloudFile = new CloudFile();
		User user = (User) request.getSession().getAttribute("user");
		try {
			String dir = "/data/" + user.getUsername() + "/";
			boolean flag = FsHelper.checkDirExist(dir);
			if (!flag) {
				flag = FsHelper.makeDir(dir);
			}
			if (flag) {
				String fileName = StringHelper.GenerateGUID() + file.getOriginalFilename();
				cloudFile.setFileName(fileName);
				cloudFile.setSize(file.getSize());
				cloudFile.setPath(dir + fileName);
				cloudFile.setUserId(user.getId());
				cloudFile.setCreateDate(DateHelper.getCurrentDateTime());
				InputStream inStream = file.getInputStream();
				FsHelper.saveFile(inStream, dir + fileName);
				fileDao.save(cloudFile);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "upload";
		}

		return "redirect:/index.do";
	}

	// delete file
	@RequestMapping(value = "delete.do", method = RequestMethod.GET)
	public String Delete(Integer id, Model model) {
		CloudFile file = new CloudFile();
		file = fileDao.findOne(id);
		try {
			boolean flag = FsHelper.deleteFile(file.getPath());
			if (flag) {
				fileDao.delete(id);
				model.addAttribute("msg", "删除成功");
			} else {
				model.addAttribute("msg", "删除失败");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("msg", "删除出现异常");
		}
		return "info";
	}

	// downlaod file
	@RequestMapping(value = "download.do", method = RequestMethod.GET)
	public String download(Integer id, HttpServletResponse response) {
		CloudFile file = new CloudFile();
		file = fileDao.findOne(id);
		response.setCharacterEncoding("utf-8");
		response.setContentType("multipart/form-data");
		response.setHeader("Content-Disposition", "attachment;fileName=" + file.getFileName());
		try {
			InputStream inStream = FsHelper.readFile(file.getPath());
			OutputStream os = response.getOutputStream();
			byte[] b = new byte[2048];
			int length;
			while ((length = inStream.read(b)) > 0) {
				os.write(b, 0, length);
			}

			// 这里主要关闭。
			os.close();
			inStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
