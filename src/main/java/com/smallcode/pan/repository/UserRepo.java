package com.smallcode.pan.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.smallcode.pan.model.User;

@Repository
public interface UserRepo extends CrudRepository<User, Integer> {

	//注意这个查询数据库里面的用户名唯一性
	@Query("select u from User u where u.Username=? and u.Password=?")
	User DoLogin(String Username, String Password);
}
