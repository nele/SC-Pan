package com.smallcode.pan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.smallcode.pan.model.*;

@Repository
public interface CloudFileRepo extends CrudRepository<CloudFile,Integer> {
        
	@Query("select f from CloudFile f where f.userId=? order by CreateDate desc")
	List<CloudFile> GetFilesByUser(Integer id);
}
