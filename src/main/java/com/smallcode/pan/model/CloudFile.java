package com.smallcode.pan.model;

import javax.persistence.*;

@Entity
@Table(name = "cloudFiles")
public class CloudFile {

	// id
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	// 文件名称
	private String fileName;

	// 路径
	private String path;

	// 创建日期
	private String CreateDate;

	// 大小
	private long size;

	// 所属用户的id
	private int userId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(String createDate) {
		CreateDate= createDate;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long l) {
		this.size = l;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
