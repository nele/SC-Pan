package com.smallcode.pan.model;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

	// id
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id;

	// �û���
	private String Username;

	// ����
	private String Password;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

}
